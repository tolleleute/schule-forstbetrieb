package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent group = FXMLLoader.load(getClass().getResource("addForrestScreen.fxml"));

        primaryStage.setTitle("\uD83C\uDF32 \uD83C\uDF32 MATSE Grün AG \uD83C\uDF32 \uD83C\uDF32");
        primaryStage.setScene(new Scene(group, 500, 400));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
