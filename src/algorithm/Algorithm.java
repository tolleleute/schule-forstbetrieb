package algorithm;

import javafx.application.Platform;
import model.Baum;
import model.Baumart;
import model.Feld;

import java.util.Random;

/**
 * Created by TBinder on 25/01/16.
 */

public class Algorithm extends Thread
{
    private Feld feld;

    private Double breite;
    private Double hoehe;

    private static  Boolean running = true;

    private final NewTreeSpeciesGenerator speciesGenerator;

    public Algorithm(Feld feld)
    {
        this.feld = feld;
        this.speciesGenerator = new NewTreeSpeciesGenerator(feld);
        this.breite = feld.getDimension().getWidth();
        this.hoehe = feld.getDimension().getHeight();
    }

    @Override
    public void run()
    {
        addTrees();
    }

    private  void addTrees()
    {
        Random r = new Random();

        while (running)
        {
            Baumart baumart = speciesGenerator.getNextSpecies();

            Double min = baumart.getRadius();
            Double x = min + ((breite - baumart.getRadius()) - min) * r.nextDouble();
            Double y = min + ((hoehe - baumart.getRadius()) - min) * r.nextDouble();

            Baum baum = new Baum(x, y, baumart);

//                for (Baum aBaum: feld.getBäume())
//                {
//                    if (!baum.checkCollision(aBaum))
                    Platform.runLater(() -> feld.addBaum(baum));
//                }

            try
            {
                Thread.sleep(10);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static void disable()
    {
        running = false;
    }
}
