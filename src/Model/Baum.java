package model;

import javafx.geometry.Point2D;
import javafx.scene.shape.Circle;

/**
 * Created by Torbe on 01.12.2015.
 */
public class Baum
{
    private final Circle circle;
    private final Baumart art;

    public Baum(Double x, Double y, Baumart art) {
        this.art = art;
        this.circle = new Circle(x, y, art.getRadius());
    }

    public Baumart getArt()
    {
        return art;
    }

    public Point2D getPosition()
    {
        return new Point2D(circle.getCenterX(), circle.getCenterY());
    }

    public Double getRadius()
    {
        return art.getRadius();
    }

    public Circle getCircle()
    {
        return circle;
    }

    public Boolean checkCollision(Baum baum)
    {
//        Shape intersect = Shape.intersect(circle, baum.getCircle());
//        return intersect.getBoundsInLocal().getWidth() != -1;
        return this.circle.getBoundsInParent().intersects(baum.getCircle().getBoundsInParent());
    }
}
